# Debian Dockerfile

## Short description
Debian is a Linux distribution that's composed entirely of free and open-source software.  
I have only added some useful tools/packages and configurations to simplify the use of this image.  

Examples of customizations :  
* Distribution upgrade
* Added of Docker labels
* Timezone configuration on `Europe/Paris`
* Added of some useful tools like vim, curl, wget, apt-utils and so on ...
* Removing useless packages version and cleaning of APT caches

## How to choose an image
This repository contains actually 3 branches :

Branch name    | Distribution names | Release versions
---------------|--------------------|-----------------
debian-wheezy  | Debian Wheezy      | 7.11
debian-jessie  | Debian Jessie      | 8.9
debian-stretch | Debian Stretch     | 9.2

## How to build an image
* Clone the repository
```shell
git clone ${this_crazy_repository}
```
* Checkout the selected branch
```shell
git checkout debian-${your_favorite_distrib}
```
* Build your own image
```shell
docker build --rm -t ${docker_image_name}:${docker_tag_name} .
```

Have fun and enjoy ;)
